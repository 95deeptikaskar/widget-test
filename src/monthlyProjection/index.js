import React, { Component } from "react";
import { Row, Col } from "antd";
import { MonthlyProjectionWraper } from "./MonthlyProjectionWraper_style_.css";
import monthlyProjectionImg from "./monthly_projection.png";

class MonthlyProjection extends Component {
    // Function For Fetching Current Month Days
    daysInThisMonth() {
        const now = new Date();
        return new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
    }

    // Function For Fetching Remaining Days
    getRemanningDays() {
        const date = new Date();
        const time = new Date(date.getTime());
        time.setMonth(date.getMonth() + 1);
        time.setDate(0);
        return time.getDate() > date.getDate()
            ? time.getDate() - date.getDate()
            : 0;
    }

    render() {
        window.addEventListener("download", e => {
            alert("download will start shortly");
        });
        return (
            <div>
                <Row gutter={0} className="monthlyProjection">
                    <Col md={8} className="data">
                        <div>11,927</div>
                        <div className="text">
                            Projected gallons for this year
                        </div>
                    </Col>
                    <Col md={10} />
                    <Col md={6} className="data">
                        <div>{this.daysInThisMonth()}</div>
                        <div className="text">Days in period</div>
                    </Col>
                    <Col md={24} className="projectionImg">
                        <img
                            src={monthlyProjectionImg}
                            alt="Monthly Projection"
                        />
                        <div className="data">
                            11,927
                            <div className="text">
                                Projected gallons for this period
                            </div>
                        </div>
                    </Col>
                    <Row gutter={0} className="bottomData">
                        <Col md={8} className="data">
                            <div>{this.getRemanningDays()}</div>
                            <div className="text">Days left in period</div>
                        </Col>
                        <Col md={9} />
                        <Col md={6} className="data">
                            <div>10,028</div>
                            <div className="text">Gallons used this period</div>
                        </Col>
                    </Row>
                </Row>
            </div>
        );
    }
}

export default MonthlyProjection;
