// var path = require('path');
// module.exports = {
//   entry: './index.js',
//   output: {
//     path: path.resolve(__dirname, 'build'),
//     filename: 'index.js',
//     libraryTarget: 'commonjs2' // THIS IS THE MOST IMPORTANT LINE! :mindblow: I wasted more than 2 days until realize this was the line most important in all this guide.
//   },
//   resolve:{
//       extensions:['js','jsx']
//   },
//   module: {
//     rules: [
//       {
//         test: /\.js|jsx\$/,
//         include: path.resolve(__dirname, 'src'),
//         exclude: /(node_modules|bower_components|build)/,
//         use: {
//           loader: 'babel-loader',
//           options: {
//             presets: ['env','react','es2015']
//           }
//         }
//       }
//     ]
//   },
//   externals: {
//     'react': 'commonjs react' // this line is just to use the React dependency of our parent-testing-project instead of using our own React.
//   }
// };

const path = require("path");

module.exports = {
  entry: "./index.js",
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "build"),
    libraryTarget: 'commonjs2'
  },
  module: {
    loaders: [
      {
        test: /.js$/,
        loader: "babel-loader",
        exclude: /(node_modules|bower_components|build)/,
        query: {
            presets: ['react', 'es2015', 'stage-2'] // load the react, es2015 babel settings
        }
      },
      {
          test: /\.css/,
          loaders: ['style-loader', 'css-loader'],
      },
      {
          test: /\.png/,
          loaders: ['file-loader'],
          options:{
            publicPath: './'
          }
      }
    ]
  },
  resolve: {
    extensions: [".js"]
  },
  externals: {
    'react': 'commonjs react' // this line is just to use the React dependency of our parent-testing-project instead of using our own React.
  }
};